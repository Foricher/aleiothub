module gitlab.com/Foricher/aleiothub

go 1.13

require (
	github.com/gdexlab/go-render v1.0.1
	github.com/gorilla/websocket v1.4.2
	github.com/jpillora/backoff v1.0.0
	github.com/luci/go-render v0.0.0-20160219211803-9a04cc21af0f // indirect
	github.com/recws-org/recws v1.2.1
	golang.org/x/tools v0.0.0-20200408032209-46bd65c8538f // indirect

)
