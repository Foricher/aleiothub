package aleiothub

import (
	"net/http"
	"strconv"
)

type DeviceFilter struct {
	Section string `json:"section,omitempty"`
}

type DeviceTopic struct {
	ID        string                   `json:"id,omitempty"`
	Selectors []map[string]interface{} `json:"selectors,omitempty"`
	Filters   []DeviceFilter           `json:"filters,omitempty"`
	Extras    []string                 `json:"extras,omitempty"`
}

type Credential struct {
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}

type Webhook struct {
	URL        string                 `json:"url,omitempty"`
	Headers    map[string]interface{} `json:"headers,omitempty"`
	Credential *Credential            `json:"credential,omitempty"`
}

type Subscriber struct {
	ID             string        `json:"id,omitempty"`
	Name           string        `json:"name,omitempty"`
	Description    string        `json:"description,omitempty"`
	TTL            int32         `json:"ttl,omitempty"`
	Tags           []string      `json:"tags,omitempty"`
	DeviceTopics   []DeviceTopic `json:"device_topics,omitempty"`
	DeviceCreated  bool          `json:"device_created,omitempty"`
	GatewayTopics  []string      `json:"gateway_topics,omitempty"`
	GatewayCreated bool          `json:"gateway_created,omitempty"`
	Webhooks       []Webhook     `json:"webhooks,omitempty"`
	ExpireTime     int64         `json:"expire_time,omitempty"`
}

func ConvertToDeviceFilter(src interface{}) *DeviceFilter {
	dd := src.(map[string]interface{})
	filter := &DeviceFilter{}
	section, ok := dd["section"].(string)
	if ok {
		filter.Section = section
	}
	return filter
}

func ConvertToDeviceTopic(src interface{}) *DeviceTopic {
	dd := src.(map[string]interface{})
	topic := &DeviceTopic{}
	id, ok := dd["id"].(string)
	if ok {
		topic.ID = id
	}
	selectors, ok := dd["selectors"].([]map[string]interface{})
	if ok {
		topic.Selectors = selectors
	}
	filters, ok := dd["filters"].([]interface{})
	if ok {
		var mFilters []DeviceFilter
		for _, v := range filters {
			res := ConvertToDeviceFilter(v)
			mFilters = append(mFilters, *res)
		}
		topic.Filters = mFilters
	}
	extras, ok := dd["extras"].([]interface{})
	if ok {
		var mExtras []string
		for _, v := range extras {
			str := v.(string)
			mExtras = append(mExtras, str)
		}
		topic.Extras = mExtras
	}
	return topic
}

func ConvertToWebhook(src interface{}) *Webhook {
	dd := src.(map[string]interface{})
	webhook := &Webhook{}
	url, ok := dd["url"].(string)
	if ok {
		webhook.URL = url
	}
	headers, ok := dd["headers"].(map[string]interface{})
	if ok {
		webhook.Headers = headers
	}
	credential, ok := dd["credential"].(map[string]interface{})
	if ok {
		username, _ := credential["username"].(string)
		password, _ := credential["password"].(string)
		webhook.Credential = &Credential{Username: username, Password: password}
	}
	return webhook
}

func ConvertToSubscribers(src interface{}) []*Subscriber {
	var _subscribers = src.([]interface{})
	var subscribers []*Subscriber = make([]*Subscriber, len(_subscribers))
	for i, d := range _subscribers {
		subscriber := ConvertToSubscriber(d)
		subscribers[i] = subscriber
	}
	return subscribers
}

func ConvertToSubscriber(src interface{}) *Subscriber {
	dd := src.(map[string]interface{})
	subscriber := &Subscriber{}
	id, ok := dd["id"].(string)
	if ok {
		subscriber.ID = id
	}
	name, ok := dd["name"].(string)
	if ok {
		subscriber.Name = name
	}
	description, ok := dd["description"].(string)
	if ok {
		subscriber.Description = description
	}
	ttl, ok := dd["ttl"].(float64)
	if ok {
		subscriber.TTL = int32(ttl)
	}
	expireTime, ok := dd["expire_time"].(float64)
	if ok {
		subscriber.ExpireTime = int64(expireTime)
	}
	tags, ok := dd["tags"].([]interface{})
	if ok {
		var mTags []string
		for _, v := range tags {
			str := v.(string)
			mTags = append(mTags, str)
		}
		subscriber.Tags = mTags
	}
	deviceTopics, ok := dd["device_topics"].([]interface{})
	if ok {
		var mDeviceTopics []DeviceTopic
		for _, v := range deviceTopics {
			mDeviceTopics = append(mDeviceTopics, *ConvertToDeviceTopic(v))
		}
		subscriber.DeviceTopics = mDeviceTopics
	}
	deviceCreated, ok := dd["device_created"].(bool)
	if ok {
		subscriber.DeviceCreated = deviceCreated
	}
	gatewayTopics, ok := dd["gateway_topics"].([]string)
	if ok {
		subscriber.GatewayTopics = gatewayTopics
	}
	gatewayCreated, ok := dd["gateway_created"].(bool)
	if ok {
		subscriber.GatewayCreated = gatewayCreated
	}
	webhooks, ok := dd["webhooks"].([]interface{})
	if ok {
		var mWebhooks []Webhook
		for _, v := range webhooks {
			mWebhooks = append(mWebhooks, *ConvertToWebhook(v))
		}
		subscriber.Webhooks = mWebhooks
	}
	return subscriber
}

// http://hassansin.github.io/request-response-pattern-using-go-channles

type SubscriberService interface {
	// Get all subscribers default max elements 200.
	// offset
	// limit  -1 illimited, 0 default 200, otherwise # elements
	// format "id", "short", "full"
	// params optional example     map[string]interface{}{
	//                         		"sort-field" : "name",
	//                         		"sort-asc" : true,
	//                      	   }
	// To convert result to []*Subscriber, apply function ConvertToSubscribers.
	GetSubscribers(offset int, limit int, format string, params map[string]interface{}) chan ResponseErr
	// Get subscribers count.
	GetSubscribersCount() chan CountErr
	// Create subscriber .
	// subscription see Subscriber struct.
	// subscriberID if empty create it otherwise if exist update it otherwise create it
	CreateSubscriber(subscription interface{}, subscriberID string) chan SuccessErr
	// Update subscriber .
	// subscriberID  subscriber identifier.
	// data see Subscriber struct.
	UpdateSubscriber(subscriberID string, data interface{}) chan SuccessErr
	// Get subscriber by Id.
	// To convert result to Subscriber, apply functions ConvertToSubscriber.
	GetSubscriber(schemaID string) chan ResponseErr
	// Delete subscriber by Id.
	DeleteSubscriber(schemaID string) chan SuccessErr
}

type subscriberWS struct {
	ClientWS ClientWS
}

func NewSubscriberService(ClientWS ClientWS) SubscriberService {
	subscriberService := &subscriberWS{ClientWS: ClientWS}
	return subscriberService
}

func (s *subscriberWS) GetSubscribers(offset int, limit int, format string, params map[string]interface{}) chan ResponseErr {
	_params := map[string]interface{}{
		"offset": offset,
		"limit":  limit,
		"format": format,
	}
	if params != nil {
		for k, v := range params {
			_params[k] = v
		}
	}
	return (s.ClientWS).SendRequestResponse("subscribers", nil, "getSubscribers", _params, nil)
}

func (s *subscriberWS) GetSubscribersCount() chan CountErr {
	countErr := make(chan CountErr)
	go func() {
		responseErr := <-(s.ClientWS).SendRequestResponse("subscribers", nil, "getSubscribersCount", nil, nil)
		if responseErr.Err != nil {
			countErr <- CountErr{Err: responseErr.Err}
		} else {
			countErr <- CountErr{Count: uint(responseErr.Data.(map[string]interface{})["count"].(float64))}
		}
	}()
	return countErr
}

func (s *subscriberWS) CreateSubscriber(subscription interface{}, subscriberID string) chan SuccessErr {
	var pSubscriberID *string
	if len(subscriberID) > 0 {
		pSubscriberID = &subscriberID
	}
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(s.ClientWS).SendRequestResponse("subscribers", pSubscriberID, "createSubscriber", nil, subscription)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (s *subscriberWS) UpdateSubscriber(subscriberID string, data interface{}) chan SuccessErr {
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(s.ClientWS).SendRequestResponse("subscribers", &subscriberID, "updateSubscriber", nil, data)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (s *subscriberWS) GetSubscriber(subscriberID string) chan ResponseErr {
	return (s.ClientWS).SendRequestResponse("subscribers", &subscriberID, "getSubscriber", nil, nil)
}

func (s *subscriberWS) DeleteSubscriber(subscriberID string) chan SuccessErr {
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(s.ClientWS).SendRequestResponse("subscribers", &subscriberID, "deleteSubscriber", nil, nil)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

//////////////////////////////////////////////////////////////////
type subscriberHTTP struct {
	ClientHTTP ClientHTTP
}

func NewSubscriberHTTPService(ClientHTTP ClientHTTP) SubscriberService {
	subscriberService := &subscriberHTTP{ClientHTTP: ClientHTTP}
	return subscriberService
}

func (s *subscriberHTTP) GetSubscribers(offset int, limit int, format string, params map[string]interface{}) chan ResponseErr {
	urlParams := "/subscribers?offset=" + strconv.Itoa(offset) + "&limit=" + strconv.Itoa(limit) + "&format=" + format
	if params != nil {
		for k, v := range params {
			urlParams += "&" + k + "=" + v.(string)
		}
	}
	return (s.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
}

func (d *subscriberHTTP) GetSubscribersCount() chan CountErr {
	urlParams := "/subscribers?count=true"
	countErr := make(chan CountErr)
	go func() {
		responseErr := <-(d.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
		if responseErr.Err != nil {
			countErr <- CountErr{Err: responseErr.Err}
		} else {
			countErr <- CountErr{Count: uint(responseErr.Data.(map[string]interface{})["count"].(float64))}
		}
	}()
	return countErr
}

func (s *subscriberHTTP) CreateSubscriber(subscription interface{}, subscriberID string) chan SuccessErr {
	urlParams := "/subscribers"
	if len(subscriberID) > 0 {
		urlParams += "/" + subscriberID
	}
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(s.ClientHTTP).SendHTTPRequest(http.MethodPost, urlParams, subscription)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (g *subscriberHTTP) UpdateSubscriber(subscriberID string, data interface{}) chan SuccessErr {
	urlParams := "/subscribers/" + subscriberID
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(g.ClientHTTP).SendHTTPRequest(http.MethodPut, urlParams, data)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (g *subscriberHTTP) GetSubscriber(subscriberID string) chan ResponseErr {
	urlParams := "/subscribers/" + subscriberID
	return (g.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
}

func (g *subscriberHTTP) DeleteSubscriber(subscriberID string) chan SuccessErr {
	urlParams := "/subscribers/" + subscriberID
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(g.ClientHTTP).SendHTTPRequest(http.MethodDelete, urlParams, nil)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}
