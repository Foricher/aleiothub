package aleiothub

import (
	"net/http"
	"strconv"
)

// http://hassansin.github.io/request-response-pattern-using-go-channles

// DeviceService Device service
type DeviceService interface {
	// Get all devices default max elements 200.
	// offset
	// limit  -1 illimited, 0 default 200, otherwise # elements
	// format "id", "short", "full"
	// params optional example     map[string]interface{}{
	//                        		"filter.type" : "iot.light.simple",
	//                        		"filter.sections.config.room.content" : "100",
	//                         		"view-config" : true,
	//                         		"sort-field" : "sections.config.room.content",
	//                         		"sort-asc" : true,
	//                      	   }
	// To convert result to []*Device, apply function ConvertToDevices.
	GetDevices(offset int, limit int, format string, params map[string]interface{}) chan ResponseErr
	// Get devices count with filters.
	// params  example     map[string]interface{
	//                        "filter.type" : "iot.light.simple" ,
	//                        "filter.sections.config.room.content" : "100" ,
	//						}
	GetDevicesCount(filters map[string]interface{}) chan CountErr
	// Create device .
	// data see Device struct.
	// params optional example     map[string]interface{
	//                               "context.mykey" : "myvalue",
	//						       }
	CreateDevice(data interface{}, params map[string]interface{}) chan SuccessErr
	// Update device .
	// deviceID  device identifier.
	// data see Device struct.
	// params optional example     map[string]interface{
	//                               "context.mykey" : "myvalue",
	//						       }
	UpdateDevice(deviceID string, data interface{}, params map[string]interface{}) chan SuccessErr
	// Get device by Id.
	// params optional example     map[string]interface{}{
	//                         		"view-config" : true,
	//                      	   }
	// To convert result to Device, apply functions ConvertToDevice.
	GetDevice(deviceID string, params map[string]interface{}) chan ResponseErr
	// Delete device by Id.
	// params optional example     map[string]interface{
	//                               "context.mykey" : "myvalue",
	//						       }
	DeleteDevice(deviceID string, params map[string]interface{}) chan SuccessErr
	// Get device section by Id.
	// params optional example     map[string]interface{}{
	//                         		"view-config" : true,
	//                      	   }
	// To convert result to Device, apply functions ConvertToDevice.
	GetDeviceSection(deviceID string, section string, params map[string]interface{}) chan ResponseErr
	// Update device section.
	// deviceID  device identifier.
	// section  section name.
	// data     example map[string]interface{}{
	//						"data": map[string]interface{}{
	//							"light_on": lightOn,
	//					},
	//           or	    DeviceSection{
	//					  Data : map[string]interface{}{
	//							    "light_on": lightOn,
	//					          },
	//                  }
	// params optional example     map[string]interface{
	//                               "context.mykey" : "myvalue",
	//						       }
	UpdateDeviceSection(deviceID string, section string, data interface{}, params map[string]interface{}) chan SuccessErr
}

type deviceWS struct {
	ClientWS ClientWS
}

// Device representing device data
type Device struct {
	ID          string                 `json:"id,omitempty"`
	Type        string                 `json:"type,omitempty"`
	Name        string                 `json:"name,omitempty"`
	Description string                 `json:"description,omitempty"`
	Enable      *bool                  `json:"enable,omitempty"`
	Tags        []string               `json:"tags,omitempty"`
	Sections    map[string]interface{} `json:"sections,omitempty"`
}

type DeviceSection struct {
	Data map[string]interface{} `json:"data,omitempty"`
}

type DeviceErr struct {
	Data *Device
	Err  *ResponseError
}

type DevicesErr struct {
	Data []*Device
	Err  *ResponseError
}

func NewDeviceService(ClientWS ClientWS) DeviceService {
	deviceService := &deviceWS{ClientWS: ClientWS}
	return deviceService
}

//ConvertToDevices  Convert to []*Device
func ConvertToDevices(src interface{}) []*Device {
	var _devices = src.([]interface{})
	var devices []*Device = make([]*Device, len(_devices))
	for i, d := range _devices {
		device := ConvertToDevice(d)
		devices[i] = device
	}
	return devices
}

func ConvertToDevice(src interface{}) *Device {
	dd := src.(map[string]interface{})
	device := &Device{}
	id, ok := dd["id"].(string)
	if ok {
		device.ID = id
	}
	_type, ok := dd["type"].(string)
	if ok {
		device.Type = _type
	}
	name, ok := dd["name"].(string)
	if ok {
		device.Name = name
	}
	description, ok := dd["description"].(string)
	if ok {
		device.Description = description
	}
	enable, ok := dd["enable"].(bool)
	if ok {
		device.Enable = &enable
	}
	tags, ok := dd["tags"].([]interface{})
	if ok {
		var mTags []string
		for _, v := range tags {
			str := v.(string)
			mTags = append(mTags, str)
		}
		device.Tags = mTags
	}
	sections, ok := dd["sections"].(map[string]interface{})
	if ok {
		device.Sections = sections
	}
	return device
}

func (d *deviceWS) GetDevices(offset int, limit int, format string, params map[string]interface{}) chan ResponseErr {
	_params := map[string]interface{}{
		"offset": offset,
		"limit":  limit,
		"format": format,
	}
	if params != nil {
		for k, v := range params {
			_params[k] = v
		}
	}
	return (d.ClientWS).SendRequestResponse("devices", nil, "getDevices", _params, nil)
	/*
		devicesErr := make(chan DevicesErr)
		go func() {
			res := <-(*d.ClientWS).SendRequestResponse("devices", nil, "getDevices", _params, nil)
			if res.Err != nil {
				devicesErr <- DevicesErr{Err: res.Err}
			} else {
				devices := ConvertToDevices(res.Data)
				devicesErr <- DevicesErr{Data: devices}
			}
		}()
		return devicesErr
	*/
}

func (d *deviceWS) GetDevicesCount(filters map[string]interface{}) chan CountErr {
	_params := map[string]interface{}{
		"count": true,
	}
	if filters != nil {
		for k, v := range filters {
			_params[k] = v
		}
	}
	countErr := make(chan CountErr)
	go func() {
		responseErr := <-(d.ClientWS).SendRequestResponse("devices", nil, "getDevices", _params, nil)
		if responseErr.Err != nil {
			countErr <- CountErr{Err: responseErr.Err}
		} else {
			countErr <- CountErr{Count: uint(responseErr.Data.(map[string]interface{})["count"].(float64))}
		}
	}()
	return countErr
}

func (d *deviceWS) CreateDevice(data interface{}, params map[string]interface{}) chan SuccessErr {
	//	return (*d.ClientWS).SendRequestResponse("devices", nil, "createDevice", params, data)
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-d.ClientWS.SendRequestResponse("devices", nil, "createDevice", params, data)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (d *deviceWS) UpdateDevice(deviceID string, data interface{}, params map[string]interface{}) chan SuccessErr {
	//	return (*d.ClientWS).SendRequestResponse("devices", &deviceID, "updateDevice", params, data)
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-d.ClientWS.SendRequestResponse("devices", &deviceID, "updateDevice", params, data)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (d *deviceWS) GetDevice(deviceID string, params map[string]interface{}) chan ResponseErr {
	return d.ClientWS.SendRequestResponse("devices", &deviceID, "getDevice", params, nil)
	/*
		deviceErr := make(chan DeviceErr)
		go func() {
			responseErr := <-(*d.ClientWS).SendRequestResponse("devices", &deviceID, "getDevice", params, nil)
			if responseErr.Err != nil {
				deviceErr <- DeviceErr{Err: responseErr.Err}
			} else {
				deviceErr <- DeviceErr{Data: ConvertToDevice(responseErr.Data)}
			}
		}()
		return deviceErr
	*/
}

func (d *deviceWS) DeleteDevice(deviceID string, params map[string]interface{}) chan SuccessErr {
	//	return (*d.ClientWS).SendRequestResponse("devices", &deviceID, "deleteDevice", params, nil)
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(d.ClientWS).SendRequestResponse("devices", &deviceID, "deleteDevice", params, nil)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (d *deviceWS) GetDeviceSection(deviceID string, section string, params map[string]interface{}) chan ResponseErr {
	_params := map[string]interface{}{
		"section": section,
	}
	if params != nil {
		for k, v := range params {
			_params[k] = v
		}
	}
	return (d.ClientWS).SendRequestResponse("devices", &deviceID, "getDeviceSection", params, nil)

}

func (d *deviceWS) UpdateDeviceSection(deviceID string, section string, data interface{}, params map[string]interface{}) chan SuccessErr {
	_params := map[string]interface{}{
		"section": section,
	}
	if params != nil {
		for k, v := range params {
			_params[k] = v
		}
	}
	//	return (*d.ClientWS).SendRequestResponse("devices", &deviceID, "updateDeviceSection", _params, data)
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(d.ClientWS).SendRequestResponse("devices", &deviceID, "updateDeviceSection", _params, data)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr

}

//////////////////////////////////////////////////////////////////
type deviceHTTP struct {
	ClientHTTP ClientHTTP
}

func addParamsToUrl(url string, params map[string]interface{}) string {
	urlParams := url
	if params != nil {
		sep := "?"
		for k, v := range params {
			urlParams += sep + k + "=" + v.(string)
			sep = "&"
		}
	}
	return urlParams
}

func NewDeviceHTTPService(ClientHTTP ClientHTTP) DeviceService {
	deviceService := &deviceHTTP{ClientHTTP: ClientHTTP}
	return deviceService
}

func (d *deviceHTTP) GetDevices(offset int, limit int, format string, params map[string]interface{}) chan ResponseErr {
	urlParams := "/devices?offset=" + strconv.Itoa(offset) + "&limit=" + strconv.Itoa(limit) + "&format=" + format
	if params != nil {
		for k, v := range params {
			urlParams += "&" + k + "=" + v.(string)
		}
	}
	return (d.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
	/*
		devicesErr := make(chan DevicesErr)
		go func() {
			res := <-(*d.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
			if res.Err != nil {
				devicesErr <- DevicesErr{Err: res.Err}
			} else {
				devices := ConvertToDevices(res.Data)
				devicesErr <- DevicesErr{Data: devices}
			}
		}()
		return devicesErr
	*/
}

func (d *deviceHTTP) GetDevicesCount(filters map[string]interface{}) chan CountErr {
	urlParams := "/devices?count=true"
	if filters != nil {
		for k, v := range filters {
			urlParams += "&" + k + "=" + v.(string)
		}
	}
	countErr := make(chan CountErr)
	go func() {
		responseErr := <-(d.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
		if responseErr.Err != nil {
			countErr <- CountErr{Err: responseErr.Err}
		} else {
			countErr <- CountErr{Count: uint(responseErr.Data.(map[string]interface{})["count"].(float64))}
		}
	}()
	return countErr
}

func (d *deviceHTTP) CreateDevice(data interface{}, params map[string]interface{}) chan SuccessErr {
	urlParams := "/devices"
	urlParams = addParamsToUrl(urlParams, params)
	//	return (*d.ClientHTTP).SendHTTPRequest(http.MethodPost, urlParams, data)
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(d.ClientHTTP).SendHTTPRequest(http.MethodPost, urlParams, nil)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (d *deviceHTTP) UpdateDevice(deviceID string, data interface{}, params map[string]interface{}) chan SuccessErr {
	urlParams := "/devices/" + deviceID
	urlParams = addParamsToUrl(urlParams, params)
	//	return (*d.ClientHTTP).SendHTTPRequest(http.MethodPut, urlParams, data)
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(d.ClientHTTP).SendHTTPRequest(http.MethodPut, urlParams, nil)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr

}

func (d *deviceHTTP) GetDevice(deviceID string, params map[string]interface{}) chan ResponseErr {
	urlParams := "/devices/" + deviceID
	urlParams = addParamsToUrl(urlParams, params)
	return (d.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
	/*
		deviceErr := make(chan DeviceErr)
		go func() {
			responseErr := <-(*d.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
			if responseErr.Err != nil {
				deviceErr <- DeviceErr{Err: responseErr.Err}
			} else {
				deviceErr <- DeviceErr{Data: ConvertToDevice(responseErr.Data)}
			}
		}()
		return deviceErr
	*/
}

func (d *deviceHTTP) DeleteDevice(deviceID string, params map[string]interface{}) chan SuccessErr {
	urlParams := "/devices/" + deviceID
	urlParams = addParamsToUrl(urlParams, params)
	//	return (*d.ClientHTTP).SendHTTPRequest(http.MethodDelete, urlParams, nil)
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(d.ClientHTTP).SendHTTPRequest(http.MethodDelete, urlParams, nil)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (d *deviceHTTP) GetDeviceSection(deviceID string, section string, params map[string]interface{}) chan ResponseErr {
	urlParams := "/devices/" + deviceID + "/sections/" + section
	urlParams = addParamsToUrl(urlParams, params)
	return (d.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
}

func (d *deviceHTTP) UpdateDeviceSection(deviceID string, section string, data interface{}, params map[string]interface{}) chan SuccessErr {
	urlParams := "/devices/" + deviceID + "/sections/" + section
	urlParams = addParamsToUrl(urlParams, params)
	//	return (*d.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, data)
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(d.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, data)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}
