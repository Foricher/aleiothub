package main

import (
	"encoding/base64"
	"log"

	"github.com/gdexlab/go-render/render"
	aleiothub "gitlab.com/Foricher/aleiothub"
)

func basicAuthHTTP(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

func main() {
	client := aleiothub.NewClientHTTP("http://localhost:9100/api", nil)
	/*
		header := http.Header{
			"Authorization": {"Basic " + basicAuthHTTP("tdd", "tdd")},
		}

		client := aleiothub.NewClientHTTP("https://iothub-dimension-data.ale-custo.com/api", &options.OptionsHTTP{
			HTTPProxy: "http://192.168.254.49:8080",
			ReqHeader: header,
		})
	*/

	deviceService := aleiothub.NewDeviceHTTPService(client)
	resp := <-deviceService.GetDevices(0, 3, "full", nil)
	if resp.Err != nil {
		log.Printf("GetDevices err %v", render.AsCode(resp.Err))
	} else {
		log.Printf("GetDevices res=%v", render.AsCode(resp.Data))
	}

	resp1 := <-deviceService.GetDevice("sga$100$shades$shade", nil)
	if resp1.Err != nil {
		log.Printf("GetDevice err %v", render.AsCode(resp1.Err))
	} else {
		log.Printf("GetDevice res=%v", render.AsCode(resp1.Data))
	}

	resp1 = <-deviceService.GetDevice("iot$tdd$tag$EE061FC8062D", nil)
	if resp1.Err != nil {
		log.Printf("GetDevice err %v", render.AsCode(resp1.Err))
	} else {
		log.Printf("GetDevice res=%v", render.AsCode(resp1.Data))
	}
}
