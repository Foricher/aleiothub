package main

import (
	"encoding/base64"
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"time"

	"github.com/gdexlab/go-render/render"

	aih "gitlab.com/Foricher/aleiothub"

	"net/http"
	_ "net/http/pprof"
)

func bToMb(b uint64) uint64 {
	return b / 1024 / 1024
}

func PrintMemUsage() {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	// For info on each, see: https://golang.org/pkg/runtime/#MemStats
	fmt.Printf("Alloc = %v BiB", m.Alloc)
	fmt.Printf("\tTotalAlloc = %v BiB", m.TotalAlloc)
	fmt.Printf("\tSys = %v biB", m.Sys)
	fmt.Printf("\tNumGC = %v\n", m.NumGC)
}

func basicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

func printDevices(devices []*aih.Device) string {
	s := ""
	for index, d := range devices {
		s += fmt.Sprintf("[%d]{%v}\n", index, d)
	}
	return s
}

func testSubscriber(client aih.ClientWS) {
	subscriberService := client.GetSubscriberService()

	resp00 := <-subscriberService.GetSubscribersCount()
	if resp00.Err != nil {
		log.Printf("GetSubscribersCount err %v", render.AsCode(resp00.Err))
	} else {
		log.Printf("GetSubscribersCount %v", resp00.Count)
	}

	subscription := aih.Subscriber{
		ID:   "go.test",
		Name: "go_test",
		TTL:  5000,
		DeviceTopics: []aih.DeviceTopic{
			aih.DeviceTopic{
				ID:        "all",
				Filters:   []aih.DeviceFilter{{Section: "state"}},
				Selectors: []map[string]interface{}{{"type": "iot.light.simple"}},
				Extras:    []string{"sections.config"},
			},
		},
		GatewayTopics:  []string{"gateway1"},
		DeviceCreated:  true,
		GatewayCreated: true,
		Webhooks: []aih.Webhook{{
			URL: "url1",
		}},
	}
	resp0 := <-subscriberService.CreateSubscriber(subscription, "")
	if resp0.Err != nil {
		log.Printf("CreateSubscriber err %v", render.AsCode(resp0.Err))
	} else {
		//		data := resp0.Data.(map[string]interface{})["id"]
		log.Printf("CreateSubscriber %v", render.AsCode(resp0.Data))
	}

	respS := <-subscriberService.GetSubscriber("go.test")
	if respS.Err != nil {
		log.Printf("GetSubscriber err %v", render.AsCode(respS.Err))
	} else {
		data := respS.Data
		subs := aih.ConvertToSubscriber(data)
		log.Printf("GetDevices %v", render.AsCode(subs))
	}

	subscribers := <-subscriberService.GetSubscribers(0, 3, "short", map[string]interface{}{
		"sort-field": "name",
		"sort-asc":   false,
	})
	if subscribers.Err != nil {
		log.Printf("GetSubscribers err %v", render.AsCode(respS.Err))
	} else {
		subs := aih.ConvertToSubscribers(subscribers.Data)
		log.Printf("GetSubscribers %v", render.AsCode(subs))
	}

}

func testDevice(client aih.ClientWS) {
	deviceService := client.GetDeviceService()
	params := map[string]interface{}{
		"filter.type": "iot.light.simple",
	}

	/*
		var respCount = <-deviceService.GetDevicesCount(params)
		if respCount.Err != nil {
			log.Printf("GetDevices Count err %v", respCount)
		} else {
			log.Printf("GetDevices Count  %v", respCount)
		}
	*/
	var respDevices = <-deviceService.GetDevices(0, 2, "full", params)
	if respDevices.Err != nil {
		log.Printf("GetDevices err %v", render.AsCode(respDevices.Err))
	} else {
		log.Printf("GetDevices %v", render.AsCode(respDevices.Data))
	}

	/*			arr := resp.Data.([]interface{})

		var devices []*aleiothub.Device = make([]*aleiothub.Device, len(arr))
		for i, d := range arr {
			device := aleiothub.ConvertToDevice(d)
			devices[i] = device
		}
		log.Printf("GetDevices %v", printDevices(devices))

		b, err := json.Marshal(resp.Data)
		if err != nil {
			fmt.Println("error:", err)
		}
		received := []aleiothub.Device{}
		json.Unmarshal(b, &received)

		log.Printf("GetDevices len=%v", len(arr))
		log.Printf("GetDevices res=%v", arr)
	}
	*/

	respDevice := <-deviceService.GetDevice("sga$100$shades$shade", nil)
	if respDevice.Err != nil {
		log.Printf("GetDevice err %v", render.AsCode(respDevice.Err))
	} else {
		device := aih.ConvertToDevice(respDevice.Data)
		room := aih.ExtractFromMap(aih.ExtractFromMap(device.Sections["config"], "room"), "content")
		log.Printf("GetDevice res=%v", room)
		log.Printf("GetDevice res=%v", device.Sections["config"].(map[string]interface{})["room"].(map[string]interface{})["content"])
	}

	resp := <-deviceService.DeleteDevice("device_id_test", nil)
	if resp.Err != nil {
		log.Printf("DeleteDevice err %v", render.AsCode(resp.Err))
	} else {
		log.Printf("DeleteDevice res=%v", render.AsCode(resp.Data))
	}

	data := &aih.Device{
		ID:   "device_id_test",
		Type: "iot.light.simple",
		Name: "test",
		Sections: map[string]interface{}{
			"config": map[string]interface{}{
				"p1": "1234",
			},
		},
	}
	/*
		bc, err := json.Marshal(data)
		if err != nil {
			fmt.Println("error:", err)
		}
		fmt.Println("bc:", string(bc))
	*/
	deviceID := ""
	resp1 := <-deviceService.CreateDevice(data, nil)
	if resp1.Err != nil {
		log.Printf("CreateDevice err %v", render.AsCode(resp1.Err))
	} else {
		deviceID = resp1.Data.ID
		log.Printf("CreateDevice res=%v id=%v", render.AsCode(resp.Data), deviceID)
	}

	respDevice = <-deviceService.GetDevice("device_id_test", nil)
	if respDevice.Err != nil {
		log.Printf("GetDevice err %v", render.AsCode(respDevice.Err))
	} else {
		log.Printf("GetDevice res=%v", render.AsCode(respDevice.Data))
	}

	data.Name = "test update"
	resp = <-deviceService.UpdateDevice(deviceID, data, nil)
	if resp.Err != nil {
		log.Printf("UpdateDevice err %v", render.AsCode(resp.Err))
	} else {
		log.Printf("UpdateDevice res=%v", render.AsCode(resp.Data))
	}

	respDevice = <-deviceService.GetDevice("bad_device_id_test", nil)
	if respDevice.Err != nil {
		log.Printf("Bad GetDevice err %v", render.AsCode(respDevice.Err))
	} else {
		log.Printf("Bad GetDevice res=%v", render.AsCode(respDevice.Data))
	}

	resp = <-deviceService.DeleteDevice("device_id_test", nil)
	if resp.Err != nil {
		log.Printf("DeleteDevice err %v", render.AsCode(resp.Err))
	} else {
		log.Printf("DeleteDevice res=%v", render.AsCode(resp.Data))
	}

}

func main() {
	go func() {
		log.Println(http.ListenAndServe("localhost:8080", nil))
	}()

	iotHubURL := os.Getenv("IOTHUB_URL")
	if iotHubURL == "" {
		piotHubURL := flag.String("IOTHUB_URL", "ws://localhost:9100/api/ws", "iot hub url")
		flag.Parse()
		iotHubURL = *piotHubURL
	}

	log.Printf("Start")
	chanEvent := make(chan map[string]interface{})
	chanConnect := make(chan bool)
	options := &aih.OptionsWS{Debug: true}
	client := aih.NewClientWS(iotHubURL, options, chanConnect, chanEvent)
	/*
			header := http.Header{
				"Authorization": {"Basic " + basicAuth("tdd", "tdd")},
			}

		//	client := aih.NewClientWS("wss://iothub.ale-custo.com/api/ws", &options.Options{
				//		HTTPProxy: "http://192.168.254.49:8080",
		//		ReqHeader: header,
		//	}, chanConnect, chanEvent)
	*/
	deviceService := client.GetDeviceService()
	go func() {
		ok := <-client.Connect()
		fmt.Println("connected=", ok)
	}()
	ticker := time.NewTicker(20 * time.Second)

	process := func() {
		testSubscriber(client)
		//testDevice(&client)

	}

	log.Printf("STARTED")
	lightOn := false
	for {
		select {
		case evt := <-chanEvent:
			log.Println("EVT=", evt)
			e := aih.ConvertToEvent(evt)
			if e.Message == "event" {
				log.Printf("event= %v", render.AsCode(e))
			}
		case connected := <-chanConnect:
			log.Println("Connected=", connected)
			if connected {
				go process()
			}
		case <-ticker.C:
			runtime.GC()
			PrintMemUsage()
			log.Printf("#goroutines: %d\n", runtime.NumGoroutine())
			if client.IsConnected() {
				//				resp := <-deviceService.GetDevice("sga$100$shades$shade", true)
				resp := <-deviceService.GetDevices(0, 2, "id", map[string]interface{}{
					"view-config": true,
				})
				if resp.Err != nil {
					log.Printf("Timer GetDevices err %v", render.AsCode(resp.Err))
				} else {
					log.Printf("Timer GetDevices res=%v", aih.ConvertToDevices(resp.Data)[0])
				}
				lightOn = !lightOn
				<-deviceService.UpdateDeviceSection("optional_unique_id", "state", map[string]interface{}{
					"data": map[string]interface{}{
						"light_on": lightOn,
					},
				}, map[string]interface{}{
					"context.k1": "v1",
				})
				/*
					<-deviceService.UpdateDeviceSection("optional_unique_id", "state", aih.DeviceSection{
						Data: map[string]interface{}{
							"light_on": !lightOn,
						},
					}, map[string]interface{}{
						"context.k1": "v1",
					})
				*/
			} else {
				log.Printf("Not Connected")
			}
		}
	}
}
