package main

import (
	"encoding/base64"
	"log"

	aleiothub "gitlab.com/Foricher/aleiothub"
)

func basicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

func main() {
	chanEvent := make(chan map[string]interface{})
	chanConnect := make(chan bool)
	client := aleiothub.NewClientWS("wss://iothub.ale-custo.com/api/ws", &aleiothub.OptionsWS{Debug: true}, chanConnect, chanEvent)
	/*
		header := http.Header{
			"Authorization": {"Basic " + basicAuth("tdd", "tdd")},
		}

		client := aleiothub.NewClientWS("wss://iothub-dimension-data.ale-custo.com/api/ws", &recws.Options{
			HTTPProxy: "http://192.168.254.49:8080",
			ReqHeader: header,
		}, chanConnect, chanEvent)
	*/
	deviceService := client.GetDeviceService()
	subcriberService := client.GetSubscriberService()

	go func() {
		<-client.Connect()
	}()
	process := func() {

		subscription := map[string]interface{}{
			"name": "subscriber go test",
			"device_topics": []map[string]interface{}{{
				"id": "sga$100$shades$shade",
				"filters": []map[string]interface{}{
					{"section": "state"},
					{"section": "actions"},
				},
			},
			},
		}
		log.Println("subscription=", subscription)
		var resp = <-subcriberService.CreateSubscriber(subscription, "")
		if resp.Err != nil {
			log.Printf("CreateSubscriber err %v", resp.Err)
		} else {
			log.Printf("CreateSubscriber res=%v", resp.Data)
		}

		respDevices := <-deviceService.GetDevices(0, 2, "id", nil)
		if respDevices.Err != nil {
			log.Printf("GetDevices err %v", resp.Err)
		} else {
			log.Printf("GetDevices res=%v", respDevices.Data)
		}
	}

	log.Printf("STARTED Subscriber")
	for {
		select {
		case evt := <-chanEvent:
			if evt["message"] == "event" {
				log.Println("EVT=", evt)
				switch evt["resource"] {
				case "devices":
					switch evt["event"] {
					case "updated_section":
						log.Println("section=", evt["section"])
					}
				}
			}
		case connected := <-chanConnect:
			log.Println("Connected=", connected)
			if connected {
				go process()
			}
		}
	}
}
