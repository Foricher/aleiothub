package main

import (
	"encoding/base64"
	"log"
	"os"
	"time"

	aleiothub "gitlab.com/Foricher/aleiothub"
)

func basicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

func createDeviceTopics(gatewayConfig map[string]interface{}) []interface{} {
	deviceTopics := []interface{}{}
	config := gatewayConfig["config"].(map[string]interface{})
	controllers := config["controllers"].([]interface{})
	for _, controller := range controllers {
		devices := controller.(map[string]interface{})["devices"].([]interface{})
		for _, device := range devices {
			deviceTopic := map[string]interface{}{
				"id": device.(map[string]interface{})["device_id"],
				"filters": []map[string]interface{}{
					{"section": "actions"},
				},
				"extras": []string{"sections.config.room.content"},
			}
			deviceTopics = append(deviceTopics, deviceTopic)
		}
	}
	return deviceTopics
}

func main() {
	chanEvent := make(chan map[string]interface{})
	chanConnect := make(chan bool)
	client := aleiothub.NewClientWS("ws://localhost:9100/api/ws", nil, chanConnect, chanEvent)
	/*
		header := http.Header{
			"Authorization": {"Basic " + basicAuth("tdd", "tdd")},
		}

		client := aleiothub.NewClientWS("wss://iothub-dimension-data.ale-custo.com/api/ws", &options.Options{
			HTTPProxy: "http://192.168.254.49:8080",
			ReqHeader: header,
		}, chanConnect, chanEvent)
	*/
	deviceService := aleiothub.NewDeviceService(client)
	gatewayService := aleiothub.NewGatewayService(client)
	subcriberService := aleiothub.NewSubscriberService(client)

	gatewayID := "knx.100"
	deviceID := "sga$100$lights$night"

	subscriberID := ""
	deviceLightState := false

	go func() {
		<-client.Connect()
	}()

	ticker := time.NewTicker(10 * time.Second)

	process := func() {

		resp := <-gatewayService.GetGateway(gatewayID)
		if resp.Err != nil {
			log.Printf("GetGateway err %v", resp.Err)
		} else {
			gatewayConfig := resp.Data.(map[string]interface{})
			deviceTopics := createDeviceTopics(gatewayConfig)
			subscription := map[string]interface{}{
				"name":           "subscriber go gateway",
				"device_topics":  deviceTopics,
				"gateway_topics": []string{gatewayID},
			}
			log.Println("subscription=", subscription)
			var resp = <-subcriberService.CreateSubscriber(subscription, subscriberID)
			if resp.Err != nil {
				log.Printf("CreateSubscriber err %v", resp.Err)
			} else {
				log.Printf("CreateSubscriber res=%v", resp.Data)
				subscriberID = resp.Data.ID
			}
		}

	}

	log.Printf("STARTED Gateway")
	for {
		select {
		case evt := <-chanEvent:
			if evt["message"] == "event" {
				log.Println("EVT=", evt)
				switch evt["resource"] {
				case "gateways":
					switch evt["event"] {
					case "stop":
						log.Println("STOP Gateway")
						os.Exit(0)
					}
				case "deleted":
					log.Println("Deleted")
				case "created", "updated":
					go process()
				case "devices":
					switch evt["event"] {
					case "updated_section":
						log.Println("section=", evt["section"])
					}
				}
			}
		case connected := <-chanConnect:
			log.Println("Connected=", connected)
			if connected {
				go process()
			}
		case <-ticker.C:
			if client.IsConnected() {
				deviceLightState = !deviceLightState
				resp := <-deviceService.UpdateDeviceSection(deviceID, "state",
					map[string]interface{}{
						"data": map[string]interface{}{
							"light_on": deviceLightState,
						}}, nil)
				if resp.Err != nil {
					log.Printf("Timer updateDeviceSection err %v", resp.Err)
				} else {
					log.Printf("Timer updateDeviceSection res=%v", resp.Data)
				}
			} else {
				log.Printf("Not Connected")
			}
		}
	}
}
