package aleiothub

import (
	"net/http"
	"regexp"
	"strconv"
)

// http://hassansin.github.io/request-response-pattern-using-go-channles

type GatewayService interface {
	// Get all gateways default max elements 200.
	// offset
	// limit  -1 illimited, 0 default 200, otherwise # elements
	// format "id", "short", "full"
	// params optional example     map[string]interface{}{
	//                         		"sort-field" : "name",
	//                         		"sort-asc" : true,
	//                      	   }
	// To convert result to []*Gateway, apply function ConvertToGateways.
	GetGateways(offset int, limit int, format string, params map[string]interface{}) chan ResponseErr
	// Get gateways count.
	GetGatewaysCount() chan CountErr
	// Create gateway .
	// data see Gateway struct.
	CreateGateway(data interface{}) chan SuccessErr
	// Update gateway .
	// gatewayID  device identifier.
	// data see Gateway struct.
	UpdateGateway(gatewayID string, data interface{}) chan SuccessErr
	// Get gateway by Id.
	// To convert result to Gateway, apply functions ConvertToGateway.
	GetGateway(gatewayID string) chan ResponseErr
	// Delete device by Id.
	DeleteGateway(gatewayID string) chan SuccessErr
	// Update gateway.
	// gatewayID  device identifier.
	// path  gateway document path.
	// data  to update.
	UpdatePartialGateway(gatewayID string, path string, data map[string]interface{}) chan SuccessErr
	// Update gateway contriller state.
	// gatewayID  device identifier.
	// controllerURL  controllerURL to update.
	// data  to update.
	SendGatewayControllerStateToHub(gatewayID string, controllerURL string, stateData map[string]interface{}) chan SuccessErr
}

type gatewayWS struct {
	ClientWS ClientWS
}

func NewGatewayService(ClientWS ClientWS) GatewayService {
	gatewayService := &gatewayWS{ClientWS: ClientWS}
	return gatewayService
}

func (g *gatewayWS) GetGateways(offset int, limit int, format string, params map[string]interface{}) chan ResponseErr {
	_params := map[string]interface{}{
		"offset": offset,
		"limit":  limit,
		"format": format,
	}
	if params != nil {
		for k, v := range params {
			_params[k] = v
		}
	}
	return (g.ClientWS).SendRequestResponse("gateways", nil, "getGateways", _params, nil)
}

func (g *gatewayWS) GetGatewaysCount() chan CountErr {
	countErr := make(chan CountErr)
	go func() {
		responseErr := <-(g.ClientWS).SendRequestResponse("gateways", nil, "getGatewaysCount", nil, nil)
		if responseErr.Err != nil {
			countErr <- CountErr{Err: responseErr.Err}
		} else {
			countErr <- CountErr{Count: uint(responseErr.Data.(map[string]interface{})["count"].(float64))}
		}
	}()
	return countErr
}

func (g *gatewayWS) CreateGateway(data interface{}) chan SuccessErr {
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(g.ClientWS).SendRequestResponse("gateways", nil, "createGateway", nil, data)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (g *gatewayWS) UpdateGateway(gatewayID string, data interface{}) chan SuccessErr {
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(g.ClientWS).SendRequestResponse("gateways", &gatewayID, "updateGateway", nil, data)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (g *gatewayWS) GetGateway(gatewayID string) chan ResponseErr {
	return (g.ClientWS).SendRequestResponse("gateways", &gatewayID, "getGateway", nil, nil)
}

func (g *gatewayWS) DeleteGateway(gatewayID string) chan SuccessErr {
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(g.ClientWS).SendRequestResponse("gateways", &gatewayID, "deleteGateway", nil, nil)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (g *gatewayWS) UpdatePartialGateway(gatewayID string, path string, data map[string]interface{}) chan SuccessErr {
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(g.ClientWS).SendRequestResponse("gateways", &gatewayID, "updatePartialGateway", map[string]interface{}{
			"path": path,
		}, data)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (g *gatewayWS) SendGatewayControllerStateToHub(gatewayID string, controllerURL string, stateData map[string]interface{}) chan SuccessErr {
	r, _ := regexp.Compile("\\.")
	str := r.ReplaceAllString(controllerURL, "_")
	return (*g).UpdatePartialGateway(gatewayID, "state.controllers."+str, stateData)
}

//////////////////////////////////////////////////////////////////
type gatewayHTTP struct {
	ClientHTTP ClientHTTP
}

func NewGatewayHTTPService(ClientHTTP ClientHTTP) GatewayService {
	gatewayService := &gatewayHTTP{ClientHTTP: ClientHTTP}
	return gatewayService
}

func (g *gatewayHTTP) GetGateways(offset int, limit int, format string, params map[string]interface{}) chan ResponseErr {
	urlParams := "/gateways?offset=" + strconv.Itoa(offset) + "&limit=" + strconv.Itoa(limit) + "&format=" + format
	if params != nil {
		for k, v := range params {
			urlParams += "&" + k + "=" + v.(string)
		}
	}
	return (g.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
}

func (g *gatewayHTTP) GetGatewaysCount() chan CountErr {
	urlParams := "/gateways?count=true"
	countErr := make(chan CountErr)
	go func() {
		responseErr := <-(g.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
		if responseErr.Err != nil {
			countErr <- CountErr{Err: responseErr.Err}
		} else {
			countErr <- CountErr{Count: uint(responseErr.Data.(map[string]interface{})["count"].(float64))}
		}
	}()
	return countErr
}

func (g *gatewayHTTP) CreateGateway(data interface{}) chan SuccessErr {
	urlParams := "/gateways"
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(g.ClientHTTP).SendHTTPRequest(http.MethodPost, urlParams, data)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (g *gatewayHTTP) UpdateGateway(gatewayID string, data interface{}) chan SuccessErr {
	urlParams := "/gateways/" + gatewayID
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(g.ClientHTTP).SendHTTPRequest(http.MethodPut, urlParams, data)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (g *gatewayHTTP) GetGateway(gatewayID string) chan ResponseErr {
	urlParams := "/gateways/" + gatewayID
	return (g.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
}

func (g *gatewayHTTP) DeleteGateway(gatewayID string) chan SuccessErr {
	urlParams := "/gateways/" + gatewayID
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(g.ClientHTTP).SendHTTPRequest(http.MethodDelete, urlParams, nil)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (g *gatewayHTTP) UpdatePartialGateway(gatewayID string, path string, data map[string]interface{}) chan SuccessErr {
	urlParams := "/gateways/" + gatewayID + "/update/" + path
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(g.ClientHTTP).SendHTTPRequest(http.MethodPut, urlParams, data)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (g *gatewayHTTP) SendGatewayControllerStateToHub(gatewayID string, controllerURL string, stateData map[string]interface{}) chan SuccessErr {
	r, _ := regexp.Compile("\\.")
	str := r.ReplaceAllString(controllerURL, "_")
	return (*g).UpdatePartialGateway(gatewayID, "state.controllers."+str, stateData)
}
