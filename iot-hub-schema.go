package aleiothub

import (
	"net/http"
	"strconv"
)

type SchemaDevice struct {
	ID          string                 `json:"id,omitempty"`
	Description string                 `json:"description,omitempty"`
	Sections    map[string]interface{} `json:"sections,omitempty"`
}

// http://hassansin.github.io/request-response-pattern-using-go-channles

type SchemaDeviceService interface {
	// Get all schemas default max elements 200.
	// offset
	// limit  -1 illimited, 0 default 200, otherwise # elements
	// format "id", "short", "full"
	// params optional example     map[string]interface{}{
	//                         		"sort-field" : "name",
	//                         		"sort-asc" : true,
	//                      	   }
	// To convert result to []*Schema, apply function ConvertToSchemas.
	GetSchemas(offset int, limit int, format string, params map[string]interface{}) chan ResponseErr
	// Get schemas count.
	GetSchemasCount() chan CountErr
	// Create schema .
	// data see Schema struct.
	CreateSchema(data interface{}) chan SuccessErr
	// Update schema .
	// schemaID  schema identifier.
	// data see Schema struct.
	UpdateSchema(schemaID string, data interface{}) chan SuccessErr
	// Get schema by Id.
	// To convert result to Schema, apply functions ConvertToSchema.
	GetSchema(schemaID string) chan ResponseErr
	// Delete schema by Id.
	DeleteSchema(schemaID string) chan SuccessErr
}

type schemaDeviceWS struct {
	ClientWS ClientWS
}

func NewSchemaDeviceService(ClientWS ClientWS) SchemaDeviceService {
	schemaService := &schemaDeviceWS{ClientWS: ClientWS}
	return schemaService
}

func (s *schemaDeviceWS) GetSchemas(offset int, limit int, format string, params map[string]interface{}) chan ResponseErr {
	_params := map[string]interface{}{
		"offset": offset,
		"limit":  limit,
		"format": format,
	}
	if params != nil {
		for k, v := range params {
			_params[k] = v
		}
	}
	return (s.ClientWS).SendRequestResponse("device-schemas", nil, "getSchemas", _params, nil)
}

func (s *schemaDeviceWS) GetSchemasCount() chan CountErr {
	countErr := make(chan CountErr)
	go func() {
		responseErr := <-(s.ClientWS).SendRequestResponse("device-schemas", nil, "getSchemasCount", nil, nil)
		if responseErr.Err != nil {
			countErr <- CountErr{Err: responseErr.Err}
		} else {
			countErr <- CountErr{Count: uint(responseErr.Data.(map[string]interface{})["count"].(float64))}
		}
	}()
	return countErr
}

func (s *schemaDeviceWS) CreateSchema(data interface{}) chan SuccessErr {
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(s.ClientWS).SendRequestResponse("device-schemas", nil, "createSchema", nil, data)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (s *schemaDeviceWS) UpdateSchema(schemaID string, data interface{}) chan SuccessErr {
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(s.ClientWS).SendRequestResponse("device-schemas", &schemaID, "updateSchema", nil, data)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (s *schemaDeviceWS) GetSchema(schemaID string) chan ResponseErr {
	return (s.ClientWS).SendRequestResponse("device-schemas", &schemaID, "getSchema", nil, nil)
}

func (s *schemaDeviceWS) DeleteSchema(schemaID string) chan SuccessErr {
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(s.ClientWS).SendRequestResponse("device-schemas", &schemaID, "deleteSchema", nil, nil)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

//////////////////////////////////////////////////////////////////
type schemaDeviceHTTP struct {
	ClientHTTP ClientHTTP
}

func NewSchemaDeviceHTTPService(ClientHTTP ClientHTTP) SchemaDeviceService {
	schemaDeviceService := &schemaDeviceHTTP{ClientHTTP: ClientHTTP}
	return schemaDeviceService
}

func (d *schemaDeviceHTTP) GetSchemas(offset int, limit int, format string, params map[string]interface{}) chan ResponseErr {
	urlParams := "/schemas/devices?offset=" + strconv.Itoa(offset) + "&limit=" + strconv.Itoa(limit) + "&format=" + format
	if params != nil {
		for k, v := range params {
			urlParams += "&" + k + "=" + v.(string)
		}
	}
	return (d.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
}

func (d *schemaDeviceHTTP) GetSchemasCount() chan CountErr {
	urlParams := "/schemas/devices?count=true"
	countErr := make(chan CountErr)
	go func() {
		responseErr := <-(d.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
		if responseErr.Err != nil {
			countErr <- CountErr{Err: responseErr.Err}
		} else {
			countErr <- CountErr{Count: uint(responseErr.Data.(map[string]interface{})["count"].(float64))}
		}
	}()
	return countErr
}

func (d *schemaDeviceHTTP) CreateSchema(data interface{}) chan SuccessErr {
	urlParams := "/schemas/devices"
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(d.ClientHTTP).SendHTTPRequest(http.MethodPost, urlParams, data)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (d *schemaDeviceHTTP) UpdateSchema(schemaID string, data interface{}) chan SuccessErr {
	urlParams := "/schemas/devices/" + schemaID
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(d.ClientHTTP).SendHTTPRequest(http.MethodPut, urlParams, data)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (d *schemaDeviceHTTP) GetSchema(schemaID string) chan ResponseErr {
	urlParams := "/schemas/devices/" + schemaID
	return (d.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
}

func (d *schemaDeviceHTTP) DeleteSchema(schemaID string) chan SuccessErr {
	urlParams := "/schemas/devices/" + schemaID
	successErr := make(chan SuccessErr)
	go func() {
		responseErr := <-(d.ClientHTTP).SendHTTPRequest(http.MethodDelete, urlParams, nil)
		if responseErr.Err != nil {
			successErr <- SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- SuccessErr{Data: ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr

}
