FROM golang:latest
RUN mkdir /app
ADD . /app/
WORKDIR /app
RUN cd examples/test && go build
CMD ["/app/examples/test/test"]
